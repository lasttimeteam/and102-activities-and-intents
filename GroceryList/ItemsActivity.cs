﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;

namespace GroceryList
{
    [Activity(Label = "Items")]
    public class ItemsActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_items);

            var lv = FindViewById<ListView>(Resource.Id.listView);

            //
            // The listview displays the collection of grocery items.
            // The adapter prepares the rows for the ListView to display.
            // We are using the Android library components ArrayAdapter and SimpleList1 to display our items.
            // - SimpleList1 is a layout file containing a single TextView
            // - Text1 is the Id of the TextView inside the SimpleListItem1 layout file.
            // ...

            lv.Adapter = new ArrayAdapter<Item>(this, Android.Resource.Layout.SimpleListItem1, Android.Resource.Id.Text1, MainActivity.Items);

            lv.ItemClick += OnItemClick;
        }

        private void OnItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Intent intent = new Intent(this, typeof(DetailsActivity));
            intent.PutExtra("ItemPosition", e.Position);

            StartActivity(intent);
        }
    }
}
