﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace GroceryList
{
    [Activity(Label = "About")]
    public class AboutActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_about);
            // Create your application here

            FindViewById<Button>(Resource.Id.learnMoreButton).Click += OnLearnMoreClick;
        }

        private void OnLearnMoreClick(object sender, EventArgs e)
        {
            var intent = new Intent();

            //
            // Use ActionView with an http Data value to launch Android's web browser Activity.
            //
            intent.SetAction(Intent.ActionView);
            intent.SetData(Android.Net.Uri.Parse("http://www.xamarin.com"));

            StartActivity(intent);
        }
    }
}
